package org.sedaq.scp.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author Pavel Seda
 */
@SpringBootApplication
public class AppSpringRun extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AppSpringRun.class);
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(AppSpringRun.class, args);
        RunAlgorithms runAlgorithms = applicationContext.getBean(RunAlgorithms.class);
        runAlgorithms.runAlgorithm();
        ((ConfigurableApplicationContext) applicationContext).close();
    }

}
