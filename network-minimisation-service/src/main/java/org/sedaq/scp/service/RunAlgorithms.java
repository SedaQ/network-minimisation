package org.sedaq.scp.service;

import org.sedaq.scp.service.algorithms.geneticalgorithm.GeneticAlgorithm;
import org.sedaq.scp.service.datapreparation.CheckSpecialCases;
import org.sedaq.scp.service.datapreparation.DataPreparation;
import org.sedaq.scp.service.representation.InputMatrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * @PropertySource("file:${path.to.config.file}") -Dpath.to.config.file=C:/School/workspace/network-minimisation/network-minimisation-service/resources/application.properties
 */
@Service
@PropertySource("classpath:application.properties")
public class RunAlgorithms {

    @Value("${csv.value.delimiter}")
    private String valueDelimiter;
    @Value("${inputmatrix.path}")
    private String inputMatrixPath;
    @Value("${multipleservice.path}")
    private String multipleServicePath;
    @Value("${usercapacities.path}")
    private String userCapacitiesPath;
    @Value("${bscapacities.path}")
    private String baseStationCapacitiesPath;
    @Value("${results.directory}")
    private String resultsDirectory;

    @Autowired
    private DataPreparation dataPreparation;
    @Autowired
    private CheckSpecialCases checkSpecialCases;
    @Autowired
    private GeneticAlgorithm geneticAlgorithm;

    public void runAlgorithm() {
        Path paths = Paths.get(inputMatrixPath);
        InputMatrix inputMatrix = dataPreparation.readData(paths, valueDelimiter, 15, false);

        List<Integer> necessaryServiceCentres = dataPreparation.prepareData(inputMatrix);
        List<Integer> multipleServiceData = dataPreparation.readMultipleService(Paths.get(multipleServicePath), valueDelimiter);
        inputMatrix.setMultipleServicesConsidered(true);
        inputMatrix.setMultipleServicesRequired(multipleServiceData);

        checkSpecialCases.multipleServicesSatisfiable(inputMatrix);

        geneticAlgorithm.startAlgorithm(inputMatrix, necessaryServiceCentres, 1.0, Paths.get(resultsDirectory));
    }
}
