package org.sedaq.scp.service.algorithms.repairoperator;

import org.sedaq.scp.service.algorithms.repairoperator.helpers.CustomerLocationCapacityUnsatisfied;
import org.sedaq.scp.service.representation.InputMatrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Pavel Seda
 */
@Service
public class RepairOperator {

    private MultipleServicesService multipleServicesService;
    private CapacitiesService capacitiesService;

    @Autowired
    public RepairOperator(MultipleServicesService multipleServicesService, CapacitiesService capacitiesService) {
        this.multipleServicesService = multipleServicesService;
        this.capacitiesService = capacitiesService;
    }

    public void repairOperator(List<Integer> possibleSolution, InputMatrix inputMatrix, Double givenPercentageOfCustomersToCover) {
        int[] possibleSolutionCovering = getPossibleSolutionCovering(possibleSolution, inputMatrix);
        if (givenPercentageOfCustomersToCover != null) {
            // Maximal Set Covering Problem with Aspects
            while (!this.givenPercentageIsTrue(inputMatrix, possibleSolutionCovering, givenPercentageOfCustomersToCover)) {
//                System.out.println("Possible Solution.." + possibleSolution);
//                System.out.println("Possible Solution Covering.." + possibleSolutionCovering);
                this.repairOperatorAlgorithm(possibleSolution, inputMatrix, possibleSolutionCovering);
            }
//            System.out.println("Pass the first check...");
            if (inputMatrix.isMultipleServicesConsidered()) {
//                System.out.println("inputMatrix.isMultipleServicesConsidered.." + possibleSolution);
                while (!multipleServicesService.multipleServicesSatisfiedWithGivenPercentage(inputMatrix, possibleSolutionCovering, givenPercentageOfCustomersToCover)) {
//                    System.out.println("in cycle...." + possibleSolution);
                    multipleServicesService.satisfyMultipleServiceAspectWithGivenPercentage(possibleSolution, inputMatrix, possibleSolutionCovering, givenPercentageOfCustomersToCover);
                }
            }
        } else {
            // Location Set Covering Problem with aspects
            while (!this.allAreTrue(inputMatrix, possibleSolutionCovering)) {
//                System.out.println("Location Set Covering Problem with aspects.." + possibleSolution);
                this.repairOperatorAlgorithm(possibleSolution, inputMatrix, possibleSolutionCovering);
            }
            // every customer is covered with the multiple services aspect (several times if needed)
            if (inputMatrix.isMultipleServicesConsidered()) {
                while (!multipleServicesService.multipleServicesSatisfied(inputMatrix, possibleSolutionCovering)) {
//                    System.out.println("Multiple Service Aspect.." + possibleSolution);
                    multipleServicesService.satisfyMultipleServiceAspect(possibleSolution, inputMatrix, possibleSolutionCovering);
                }
            }
            // check capacities
            if (inputMatrix.isCapacitiesConsidered()) {
                while (!capacitiesService.compareCapacities(possibleSolution, inputMatrix)) {
                    capacitiesService.addAdditionalServiceWithHighCapacity(possibleSolution, inputMatrix);
                }
                CustomerLocationCapacityUnsatisfied customerLocationCapacityUnsatisfied = capacitiesService.customerLocationsCapacitySatisfied(possibleSolution, inputMatrix);
                while (!customerLocationCapacityUnsatisfied.isSatisfied()) {
                    int customerLocationUnsatisfied = customerLocationCapacityUnsatisfied.getCustomerLocationId();
                    // provide better method
                    capacitiesService.addAdditionalServiceWithHighCapacityCoveringUncoveredCustomerLocation(possibleSolution, inputMatrix, customerLocationUnsatisfied);
                }
            }
        }
    }

    public int[] getPossibleSolutionCovering(List<Integer> possibleSolution, InputMatrix inputMatrix) {
        int[][] matrix = inputMatrix.getMatrix();
        int[] possibleSolutionCovering = new int[matrix[0].length]; // initialize covering with all 0

        for (int i = 0; i < possibleSolution.size(); i++) {
            if (possibleSolution.get(i) == 1) {
                int[] inputDataColumns = matrix[i];
                for (int j = 0; j < inputDataColumns.length; j++) {
                    if (inputDataColumns[j] == 1) {
                        possibleSolutionCovering[j] = possibleSolutionCovering[j] + 1; // this customer location is covered by this possible solution
                    }
                }
            }
        }
        return possibleSolutionCovering;
    }

    private int[] repairOperatorAlgorithm(List<Integer> possibleSolution, InputMatrix inputMatrix, int[] possibleSolutionCovering) {
        // sort service centres not in solution by covering statistics (service centre which covers the most uncovered customer locations will be added to the solution)
        Map<Integer, Integer> coverStatistics = getCoverStatistics(possibleSolution, inputMatrix, possibleSolutionCovering);
        Map<Integer, Integer> coverStatisticsSortedByValue = getCoverStatisticsSortedByValue(coverStatistics);
        addServiceToTheSolution(possibleSolution, inputMatrix, possibleSolutionCovering, coverStatisticsSortedByValue);
        return possibleSolutionCovering;
    }

    private void addServiceToTheSolution(List<Integer> possibleSolution, InputMatrix inputMatrix,
                                         int[] possibleSolutionCovering, Map<Integer, Integer> coverStatisticsSortedByNumberOfAdditionalCoverings) {
        Iterator<Map.Entry<Integer, Integer>> iterator = coverStatisticsSortedByNumberOfAdditionalCoverings
                .entrySet()
                .iterator();
        if (iterator.hasNext()) {
            Map.Entry<Integer, Integer> entry = iterator.next();
            if (entry != null) { // entry does not contain any values, probably all the service centres are in the solution
                Integer serviceCentreToAdd = entry.getKey();
                // add specific service centre to the solution
                possibleSolution.set(serviceCentreToAdd, 1);

                //remove already covered customer locations from uncovered customer locations list
                int[] serviceCentreRowInMatrix = inputMatrix.getMatrix()[serviceCentreToAdd];
                for (int j = 0; j < serviceCentreRowInMatrix.length; j++) {
                    if (serviceCentreRowInMatrix[j] == 1) {
                        possibleSolutionCovering[j] = possibleSolutionCovering[j] + 1;
                    }
                }
            }
        }
    }

    /**
     * In the case that two service centres covers same number of uncovered customers then select the service centre that covers more customer locations
     *
     * @param coverStatistics
     * @return
     */
    private Map<Integer, Integer> getCoverStatisticsSortedByValue(Map<Integer, Integer> coverStatistics) {
        return coverStatistics.entrySet()
                .stream()
                .sorted((Map.Entry.<Integer, Integer>comparingByValue().reversed()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    private Map<Integer, Integer> getCoverStatistics(List<Integer> possibleSolution, InputMatrix inputMatrix, int[] possibleSolutionCovering) {
        Map<Integer, Integer> coverStatistics = new HashMap<>();
        for (int i = 0; i < possibleSolution.size(); i++) {
            int coversUncoveredCustomerLocationsSum = 0;
            if (possibleSolution.get(i) == 0) {
                int[] inputDataColumns = inputMatrix.getMatrix()[i];
                for (int j = 0; j < inputDataColumns.length; j++) {
                    if (inputDataColumns[j] == 1 && possibleSolutionCovering[j] == 0) {
                        coversUncoveredCustomerLocationsSum++;
                    }
                }
                coverStatistics.put(i, coversUncoveredCustomerLocationsSum);
            }
        }
        return coverStatistics;
    }

    // LSCP 100% coverage
    public boolean allAreTrue(InputMatrix inputMatrix, int[] possibleSolutionCovering) {
        for (int i = 0; i < possibleSolutionCovering.length; i++) {
            if (possibleSolutionCovering[i] < 1) {
                return false;
            }
        }
        return true;
    }

    // MSCP - given percetange coverage
    public boolean givenPercentageIsTrue(InputMatrix inputMatrix, int[] possibleSolutionCovering, double percentageToCover) {
        if (percentageToCover > 1 || percentageToCover < 0)
            throw new IllegalArgumentException("Percentage to cover must be between 0 and 1, e.g., 0.6 which refers to 60% cover percentage.");
        int necessaryCoverage = (int) Math.round(possibleSolutionCovering.length * percentageToCover);
        int solutionSum = 0;
        for (int i = 0; i < possibleSolutionCovering.length; i++) {
            if (possibleSolutionCovering[i] >= 1) {
                solutionSum = solutionSum + 1;
            }
        }
        if (solutionSum >= necessaryCoverage) {
            return true;
        } else {
            return false;
        }
    }

}
