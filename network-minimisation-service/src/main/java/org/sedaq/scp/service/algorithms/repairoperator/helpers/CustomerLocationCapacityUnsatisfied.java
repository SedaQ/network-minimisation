package org.sedaq.scp.service.algorithms.repairoperator.helpers;

public class CustomerLocationCapacityUnsatisfied {
    private boolean satisfied;
    private int customerLocationId;

    public CustomerLocationCapacityUnsatisfied(boolean satisfied) {
        this.satisfied = satisfied;
    }

    public CustomerLocationCapacityUnsatisfied(boolean satisfied, int customerLocationId) {
        this.satisfied = satisfied;
        this.customerLocationId = customerLocationId;
    }

    public boolean isSatisfied() {
        return satisfied;
    }

    public void setSatisfied(boolean satisfied) {
        this.satisfied = satisfied;
    }

    public int getCustomerLocationId() {
        return customerLocationId;
    }

    public void setCustomerLocationId(int customerLocationId) {
        this.customerLocationId = customerLocationId;
    }

    @Override
    public String toString() {
        return "CustomerLocationCapacityUnsatisfied{" +
                "satisfied=" + satisfied +
                ", customerLocationId=" + customerLocationId +
                '}';
    }
}
