package org.sedaq.scp.service;

import org.sedaq.scp.service.algorithms.repairoperator.CapacitiesService;
import org.sedaq.scp.service.algorithms.repairoperator.MultipleServicesService;
import org.sedaq.scp.service.algorithms.repairoperator.RepairOperator;
import org.sedaq.scp.service.algorithms.geneticalgorithm.GeneticAlgorithm;
import org.sedaq.scp.service.datapreparation.DataPreparation;
import org.sedaq.scp.service.impl.FindBestSolution;
import org.sedaq.scp.service.impl.StartAlgorithms;
import org.sedaq.scp.service.io.ReadInputData;
import org.sedaq.scp.service.io.WriteToCSVFile;
import org.sedaq.scp.service.datapreparation.CheckSpecialCases;
import org.sedaq.scp.service.representation.InputMatrix;
import org.sedaq.scp.service.utils.MatrixUtils;

import java.nio.file.*;
import java.util.List;

/**
 * @author Pavel Seda
 */
public class App {


    public static final String FILE_PREFIX = "./network-minimisation-service/src/main/resources/data-simulations-jirka/";

    /**
     * arg[0] - input matrix
     * arg[1] - multiple service
     * arg[2] - service centres capacities
     * arg[3] - customer location capacities
     *
     * @param args
     */
    public static void main(String[] args) {
        MatrixUtils matrixUtils = new MatrixUtils();
        ReadInputData readInputData = new ReadInputData();
        WriteToCSVFile writeToCSVFile = new WriteToCSVFile();
        CheckSpecialCases checkSpecialCases = new CheckSpecialCases();
        RepairOperator repairOperator = new RepairOperator(new MultipleServicesService(), new CapacitiesService());
        StartAlgorithms startAlgorithms = new StartAlgorithms(new FindBestSolution());

        Path inputMatrixCsv = Paths.get(FILE_PREFIX + "matrix_8.3.2020-23_41_34.csv");

        String valueDelimiter = ",";
        DataPreparation dataPreparation = new DataPreparation(readInputData, matrixUtils, checkSpecialCases);
        InputMatrix inputMatrix = dataPreparation.readData(inputMatrixCsv, valueDelimiter, 15, false);
        // capacities consideration
        Path serviceCentreCapacityCsv = Paths.get(FILE_PREFIX + "bsCap_8.3.2020-23_41_34.csv");
        Path customerLocationCapacityCsv = Paths.get(FILE_PREFIX + "userCap_8.3.2020-23_41_34.csv");
        dataPreparation.readServicesAndCustomerCapacities(inputMatrix, serviceCentreCapacityCsv, customerLocationCapacityCsv, valueDelimiter);
        inputMatrix.setCapacitiesConsidered(true);
        checkSpecialCases.capacitiesSatisfiable(inputMatrix);
        checkSpecialCases.compareCapacities(inputMatrix);

        // for the use-case that every customer location needs to be covered directly by one service centre (multiple services problem)
        Path multipleServiceCsv = Paths.get(FILE_PREFIX + "multipleService_8.3.2020-23_41_34.csv");
        List<Integer> multipleServiceData = dataPreparation.readMultipleService(multipleServiceCsv, valueDelimiter);
        System.out.println("Multiple service data.." + multipleServiceData);
        inputMatrix.setMultipleServicesRequired(multipleServiceData);
        inputMatrix.setMultipleServicesConsidered(true);
        //inputMatrix.setMultipleServicesForAllLocations(1);

        checkSpecialCases.multipleServicesSatisfiable(inputMatrix);

        List<Integer> necessaryServiceCentres = dataPreparation.prepareData(inputMatrix);

        Path resultsBasePathGA = Paths.get("./network-minimisation-service/src/main/resources/results/ga/");
////        Path resultsBasePathSA = Paths.get("./network-minimisation-service/src/main/resources/results/sa");
////        Path resultsBasePathTS = Paths.get("./network-minimisation-service/src/main/resources/results/ts");
////        Path resultsBasePathHC = Paths.get("./network-minimisation-service/src/main/resources/results/hc");
////        Path resultsBasePathCS = Paths.get("./network-minimisation-service/src/main/resources/results/cs");
////        Path resultsBasePathDE = Paths.get("./network-minimisation-service/src/main/resources/results/de");
//
        int numberOfThreads = 1;
        startAlgorithms.startAlgorithmsConcurrently(new GeneticAlgorithm(writeToCSVFile, repairOperator), numberOfThreads, inputMatrix, necessaryServiceCentres, 0.9, resultsBasePathGA);
//        startAlgorithms.startAlgorithmsConcurrently(new DifferentialEvolution(writeToCSVFile, repairOperator), numberOfThreads, inputMatrix, necessaryServiceCentres, 1.0, resultsBasePathDE);
        // startAlgorithms.startAlgorithmsConcurrently(new CuckooSearch(writeToCSVFile, repairOperator), numberOfThreads, inputMatrix, necessaryServiceCentres, 1.0, resultsBasePathCS);
        //   startAlgorithms.startAlgorithmsConcurrently(new HillClimbing(writeToCSVFile, repairOperator), numberOfThreads, inputMatrix, necessaryServiceCentres, 1.0, resultsBasePathHC);
//        startAlgorithms.startAlgorithmsConcurrently(new SimulatedAnnealing(writeToCSVFile, repairOperator), numberOfThreads, inputMatrix, necessaryServiceCentres, 1.0, resultsBasePathSA);
//        startAlgorithms.startAlgorithmsConcurrently(new TabuSearch(writeToCSVFile, repairOperator), numberOfThreads, inputMatrix, necessaryServiceCentres, 1.0, resultsBasePathTS);
    }

}
