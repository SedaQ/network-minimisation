package org.sedaq.scp.service.algorithms.repairoperator;

import org.sedaq.scp.service.algorithms.repairoperator.helpers.CustomerLocationCapacityUnsatisfied;
import org.sedaq.scp.service.algorithms.repairoperator.helpers.TheBestValueWithIndex;
import org.sedaq.scp.service.exceptions.RepairOperatorException;
import org.sedaq.scp.service.representation.InputMatrix;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class CapacitiesService {

    // sum of service centre capacities compare to sum of customer location capacities comparison
    
    public boolean compareCapacities(List<Integer> possibleSolution, InputMatrix inputMatrix) {
        List<BigDecimal> serviceCentreCapacities = inputMatrix.getServiceCentresCapacities();

        BigDecimal selectedServiceCentreCapacities = BigDecimal.ZERO;
        for (int i = 0; i < possibleSolution.size(); i++) {
            if (possibleSolution.get(i) == 1) {
                selectedServiceCentreCapacities = selectedServiceCentreCapacities.add(serviceCentreCapacities.get(i));
            }
        }
        BigDecimal customerLocationCapacities = inputMatrix.getCustomerLocationsCapacities().stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        // check if sum of service centre capacities is higher than customer location capacities
        if (selectedServiceCentreCapacities.subtract(customerLocationCapacities).compareTo(BigDecimal.ZERO) >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public void addAdditionalServiceWithHighCapacity(List<Integer> possibleSolution, InputMatrix inputMatrix) {
        TheBestValueWithIndex theBestValueWithIndex = new TheBestValueWithIndex(BigDecimal.ZERO, null);
        for (int i = 0; i < possibleSolution.size(); i++) {
            if (possibleSolution.get(i) == 0) {
                if (inputMatrix.getServiceCentresCapacities().get(i).compareTo(theBestValueWithIndex.getValue()) > 0) {
                    theBestValueWithIndex.setValue(inputMatrix.getServiceCentresCapacities().get(i));
                    theBestValueWithIndex.setIndex(i);
                }
            }
        }
        if (theBestValueWithIndex.getIndex() != null) {
            possibleSolution.set(theBestValueWithIndex.getIndex(), 1);
        } else {
            throw new RepairOperatorException("It is not possible to add additional service.. all the services were added.");
        }
    }

    // customer locations capacities compare to service centre capacities

    public CustomerLocationCapacityUnsatisfied customerLocationsCapacitySatisfied(List<Integer> possibleSolution, InputMatrix inputMatrix) {
        List<BigDecimal> requiredCustomerLocationCapacities = inputMatrix.getCustomerLocationsCapacities();
        List<BigDecimal> availableServiceCentreCapacities = inputMatrix.getServiceCentresCapacities();
        int[][] reachabilityMatrix = inputMatrix.getMatrix();

        BigDecimal sum = BigDecimal.ZERO; // big decimal is immutable.. maybe consider double
        for (int j = 0; j < reachabilityMatrix[j].length; j++) {
            for (int i = 0; i < reachabilityMatrix.length; i++) {
                if (possibleSolution.get(i) == 1) {
                    if (reachabilityMatrix[i][j] == 1) {
                        sum = sum.add(availableServiceCentreCapacities.get(i));
                    }
                }
            }
            if (sum.compareTo(requiredCustomerLocationCapacities.get(j)) < 1) {
                return new CustomerLocationCapacityUnsatisfied(false, j);
            }
            sum = BigDecimal.ZERO;
        }
        return new CustomerLocationCapacityUnsatisfied(true);
    }

    public void addAdditionalServiceWithHighCapacityCoveringUncoveredCustomerLocation(List<Integer> possibleSolution, InputMatrix inputMatrix, int uncoveredCustomerLocation) {
        int[][] reachabilityMatrix = inputMatrix.getMatrix();
        TheBestValueWithIndex theBestValueWithIndex = new TheBestValueWithIndex(BigDecimal.ZERO, null);
        for (int i = 0; i < reachabilityMatrix.length; i++) {
            if (reachabilityMatrix[i][uncoveredCustomerLocation] == 0) {
                if (inputMatrix.getServiceCentresCapacities().get(i).compareTo(theBestValueWithIndex.getValue()) > 0) {
                    theBestValueWithIndex.setValue(inputMatrix.getServiceCentresCapacities().get(i));
                    theBestValueWithIndex.setIndex(i);
                }
            }
        }
        possibleSolution.set(theBestValueWithIndex.getIndex(), 1);
    }

}
