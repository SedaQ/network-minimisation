package org.sedaq.scp.service.algorithms.repairoperator;

import org.sedaq.scp.service.representation.InputMatrix;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MultipleServicesService {

    public void satisfyMultipleServiceAspect(List<Integer> possibleSolution, InputMatrix inputMatrix, int[] possibleSolutionCovering) {
        for (int j = 0; j < possibleSolutionCovering.length; j++) {
            if (possibleSolutionCovering[j] < inputMatrix.getMultipleServicesRequired().get(j)) {
                do {
                    // add services that cover this customer locations to satisfy multiple services aspect
                    int[][] matrix = inputMatrix.getMatrix();
                    for (int i = 0; i < matrix.length; i++) {
                        // column i from input matrix, possible solution m (row) included or not
                        if (matrix[i][j] == 1 && possibleSolution.get(i) == 0) {
                            possibleSolution.set(i, 1);
                            possibleSolutionCovering[j] = possibleSolutionCovering[j] + 1;
                            if (possibleSolutionCovering[j] >= inputMatrix.getMultipleServicesRequired().get(j)) {
                                break;
                            }
                        }
                    }
                }
                while (possibleSolutionCovering[j] < inputMatrix.getMultipleServicesRequired().get(j));
                // the suitable check before that loop is necessary otherwise it reaches the infinite loop
            }
        }
    }

    public boolean multipleServicesSatisfied(InputMatrix inputMatrix, int[] possibleSolutionCovering) {
        for (int i = 0; i < possibleSolutionCovering.length; i++) {
            if (possibleSolutionCovering[i] < inputMatrix.getMultipleServicesRequired().get(i)) {
                System.out.println("Customer location: " + i + " not covered by: " + (inputMatrix.getMultipleServicesRequired().get(i) - possibleSolutionCovering[i]) + " services.");
                return false;
            }
        }
        return true;
    }

    /**
     * Consider changing the implementation to add only customer locations that required less service centres to cover
     *
     * @param possibleSolution
     * @param inputMatrix
     * @param possibleSolutionCovering
     * @param percentageToCover
     */
    public void satisfyMultipleServiceAspectWithGivenPercentage(List<Integer> possibleSolution, InputMatrix inputMatrix, int[] possibleSolutionCovering, double percentageToCover) {
        for (int j = 0; j < possibleSolutionCovering.length; j++) {
            if (possibleSolutionCovering[j] < inputMatrix.getMultipleServicesRequired().get(j)) {
                do {
                    // add services that cover this customer locations to satisfy multiple services aspect
                    int[][] matrix = inputMatrix.getMatrix();
                    for (int i = 0; i < matrix.length; i++) {
                        // column i from input matrix, possible solution m (row) included or not
                        if (matrix[i][j] == 1 && possibleSolution.get(i) == 0) {
                            possibleSolution.set(i, 1);
                            possibleSolutionCovering[j] = possibleSolutionCovering[j] + 1;
                            if (possibleSolutionCovering[j] >= inputMatrix.getMultipleServicesRequired().get(j)) {
                                break;
                            }
                        }
                    }
                    //System.out.println("In cycle.. satisfyMultipleServiceAspectWithGivenPercentage");
                }
                while (possibleSolutionCovering[j] < inputMatrix.getMultipleServicesRequired().get(j));
                // the suitable check before that loop is necessary otherwise it reaches the infinite loop
                if (multipleServicesSatisfiedWithGivenPercentage(inputMatrix, possibleSolutionCovering, percentageToCover)) {
                    break;
                }
            }
        }
    }

    public boolean multipleServicesSatisfiedWithGivenPercentage(InputMatrix inputMatrix, int[] possibleSolutionCovering, double percentageToCover) {
        if (percentageToCover > 1 || percentageToCover < 0)
            throw new IllegalArgumentException("Percentage to cover must be between 0 and 1, e.g., 0.6 which refers to 60% cover percentage.");
        int necessaryCoverage = (int) Math.round(possibleSolutionCovering.length * percentageToCover);
        int solutionSum = 0;
        for (int i = 0; i < possibleSolutionCovering.length; i++) {
            if (possibleSolutionCovering[i] >= inputMatrix.getMultipleServicesRequired().get(i)) {
                solutionSum = solutionSum + 1;
            }
        }
        if (solutionSum >= necessaryCoverage) {
            return true;
        } else {
            return false;
        }
    }

}
