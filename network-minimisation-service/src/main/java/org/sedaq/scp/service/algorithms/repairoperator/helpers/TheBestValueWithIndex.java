package org.sedaq.scp.service.algorithms.repairoperator.helpers;

import java.math.BigDecimal;

public class TheBestValueWithIndex {

    private BigDecimal value;
    private Integer index;


    public TheBestValueWithIndex(BigDecimal value, Integer index) {
        this.value = value;
        this.index = index;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "TheBestValueWithIndex{" +
                "value=" + value +
                ", index=" + index +
                '}';
    }
}
