package org.sedaq.scp.service.datapreparation;

import org.sedaq.scp.service.exceptions.DataPreparationException;
import org.sedaq.scp.service.exceptions.SpecialCasesException;
import org.sedaq.scp.service.io.ReadInputData;
import org.sedaq.scp.service.representation.InputMatrix;
import org.sedaq.scp.service.utils.MatrixUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DataPreparation {

    private MatrixUtils matrixUtils;
    private CheckSpecialCases checkSpecialCases;
    private ReadInputData readInputData;

    @Autowired
    public DataPreparation(ReadInputData readInputData, MatrixUtils matrixUtils, CheckSpecialCases checkSpecialCases) {
        this.readInputData = readInputData;
        this.matrixUtils = matrixUtils;
        this.checkSpecialCases = checkSpecialCases;
    }

    public InputMatrix readData(Path pathToInputMatrix, String delimiter, int dMax, boolean needDistanceToReachabilityMatrixConversion) {
        InputMatrix inputMatrix = readInputData.readInputDataInputMatrix(pathToInputMatrix, delimiter);
        if (needDistanceToReachabilityMatrixConversion) {
            inputMatrix.setMatrix(matrixUtils.convertDistanceMatrixToReachabilityMatrix(inputMatrix.getMatrix(), dMax));
//            writeReachAbilityMatrixFromDistanceMatrix(inputMatrix);
        }
        return inputMatrix;
    }

    public List<Integer> readMultipleService(Path multipleServiceCsv, String delimiter) {
        try (Stream<String> multipleService = Files.lines(multipleServiceCsv)) {
            return multipleService
                    .flatMap(line -> Stream.of(line.split(delimiter)))
                    .map(Integer::valueOf)
                    .collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            throw new DataPreparationException(e);
        }
    }

    public InputMatrix readServicesAndCustomerCapacities(InputMatrix inputMatrix, Path servicesCapacities, Path customerCapacities, String delimiter) {
        Objects.requireNonNull(inputMatrix, "Input matrix must be initialized before calling method: readServicesAndCustomerCapacities");
        Objects.requireNonNull(delimiter, "Please selecte delimiter for files");
        inputMatrix.setServiceCentresCapacities(readCapacities(servicesCapacities, delimiter));
        inputMatrix.setCustomerLocationsCapacities(readCapacities(customerCapacities, delimiter));
        return inputMatrix;
    }

    private List<BigDecimal> readCapacities(Path fileWithCapacities, String delimiter) {
        try (Stream<String> capacities = Files.lines(fileWithCapacities)) {
            return capacities
                    .flatMap(line -> Stream.of(line.split(delimiter)))
                    .map(BigDecimal::new)
                    .collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            throw new DataPreparationException(e);
        }
    }

    private void writeReachAbilityMatrixFromDistanceMatrix(InputMatrix inputMatrix) {
        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get("./network-minimisation-service/src/main/resources/reach-ability-matrix-distance.csv"))) {
            int[][] reachabilityMatrix = inputMatrix.getMatrix();
            for (int i = 0; i < reachabilityMatrix.length; i++) {
                for (int j = 0; j < reachabilityMatrix[i].length; j++) {
                    bw.write(reachabilityMatrix[i][j] + ",");
                }
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Integer> checkImmediateSolutionAndDataValidity(InputMatrix inputMatrix) {
        if (!checkSpecialCases.serviceCentreCoverSomething(inputMatrix.getMatrix())) {
            throw new SpecialCasesException("Reachability matrix contains only zeros, service centres does not cover anything. Please consider different communication technology or increase dMax");
        }
        if (!checkSpecialCases.isDMaxValid(inputMatrix.getMatrix())) {
            throw new SpecialCasesException("Parameter dMax is not valid.");
        }
        List<Integer> serviceCentresAsImmediateSolution = checkSpecialCases.serviceCentresAsImmediateSolution(inputMatrix.getMatrix());
        if (serviceCentresAsImmediateSolution != null && serviceCentresAsImmediateSolution.size() > 0) {
            return serviceCentresAsImmediateSolution;
        }
        return Collections.emptyList();
    }

    public List<Integer> prepareData(InputMatrix inputMatrix) {
        List<Integer> necessaryServiceCentres = checkSpecialCases.necessaryServiceCentres(inputMatrix.getMatrix());
//        List<Integer> serviceCentresToRemove = checkSpecialCases.serviceCentresNotCoverAnything(inputMatrix.getMatrix());
//        inputMatrix.setMatrix(matrixUtils.removeGivenServiceCentres(inputMatrix.getMatrix(), serviceCentresToRemove));
//        inputMatrix.setMatrix(matrixUtils.convertReachabilityMatrixToCapacityMatrix(inputMatrix.getMatrix(), inputMatrix.getCustomerLocationsCapacities()));

        return necessaryServiceCentres;
    }


}
