package org.sedaq.scp.service.exceptions;

public class DataPreparationException extends RuntimeException {

    public DataPreparationException() {
    }

    public DataPreparationException(String message) {
        super(message);
    }

    public DataPreparationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataPreparationException(Throwable cause) {
        super(cause);
    }

    public DataPreparationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
